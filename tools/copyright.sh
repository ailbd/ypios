#
# Copyright (c) 2023 Jianjun Yue and letscode contributors.
# letscode is licensed under Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
# http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
#

#!/bin/bash

set -u

srcfiles=$(find . -type f -name "*.c" -o -name "*.cpp" -o -name "*.h")
assfiles=$(find . -type f -name "*.S")

cmd_insert_MuLan_PSL_v2="sed -i '1i\/\*\n \* Copyright (c) 2023 Jianjun Yue and ypios contributors.\n \* letscode is licensed under Mulan PSL v2.\n \* You can use this software according to the terms and conditions of the Mulan PSL v2.\n \* You may obtain a copy of Mulan PSL v2 at:\n \* http://license.coscl.org.cn/MulanPSL2\n \* THIS SOFTWARE IS PROVIDED ON AN \"AS IS\" BASIS, WITHOUT WARRANTIES OF ANY KIND,\n \* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,\n \* MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.\n \* See the Mulan PSL v2 for more details.\n \*/\n'"

gn_insert_MuLan_PSL_v2="sed -i '1i\#\n# Copyright (c) 2023 Jianjun Yue and ypios contributors.\n# letscode is licensed under Mulan PSL v2.\n# You can use this software according to the terms and conditions of the Mulan PSL v2.\n# You may obtain a copy of Mulan PSL v2 at:\n# http://license.coscl.org.cn/MulanPSL2\n# THIS SOFTWARE IS PROVIDED ON AN \"AS IS\" BASIS, WITHOUT WARRANTIES OF ANY KIND,\n# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,\n# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.\n# See the Mulan PSL v2 for more details.\n#\n'"

for file in ${srcfiles[@]}
do
    eval $cmd_insert_MuLan_PSL_v2 $file
    echo "$file copyright substitude."
done

for assfile in ${assfiles[@]}
do
    eval $gn_insert_MuLan_PSL_v2 $assfile
    echo "$file copyright substitude."
done
