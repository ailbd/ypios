#
# Copyright (c) 2023 Jianjun Yue and ypios contributors.
# letscode is licensed under Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
# http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
#

#!/bin/bash

# Check if the script is run as root
if [ "$EUID" -ne 0 ]; then
  echo "Please run as root"
  exit 1
fi

set -e
if [ $# -ne 1 ]; then
  echo "no disk device specified"
  exit 1
else
  echo "WARNING! You are formating device $1"
  read -p "Press Enter to continue."

  sdcard=$1

  echo "Script resumed after the pause."
fi

# proceed to execute whatever this command succeeds or fails.
set +e
umount $sdcard*
set -e

# delete all partitions
sfdisk --delete $sdcard

# make a 100MiB partition
# Create the first partition
echo "2048,204800,b," | sfdisk --no-reread --wipe always "$sdcard"

# Create the second partition
echo ",204800,b," | sfdisk --append --no-reread --wipe always "$sdcard"

echo ",,83," | sfdisk --append --no-reread --wipe always "$sdcard"


# format the partition
part="${sdcard}1"  # normal /dev/sdb1 format
if [[ $sdcard == /dev/mmcblk* ]]; then
  part="${sdcard}p1"
fi
sfdisk --no-reread --part-type $sdcard 1 b
sfdisk --no-reread --part-label $sdcard 1 boot
mkfs.vfat -F 32 -n boot $part

# format the partition
part="${sdcard}2"  # normal /dev/sdb2 format
if [[ $sdcard == /dev/mmcblk* ]]; then
  part="${sdcard}p2"
fi
sfdisk --no-reread --part-type $sdcard 2 b
sfdisk --no-reread --part-label $sdcard 2 kernel
mkfs.vfat -F 32 -n kernel $part

part="${sdcard}3"  # normal /dev/sdb3 format
if [[ $sdcard == /dev/mmcblk* ]]; then
  part="${sdcard}p3"
fi
sfdisk --no-reread --part-type $sdcard 3 83
sfdisk --no-reread --part-label $sdcard 3 root
mkfs.ext4 -L root $part