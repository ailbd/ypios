#
# Copyright (c) 2023 Jianjun Yue and ypios contributors.
# letscode is licensed under Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
# http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
#

ifeq ("$(origin O)", "command line")
	OUTPUT_DIR := $(O)
endif

ifeq ($(OUTPUT_DIR),)
	OUTPUT_DIR := ./build
endif # ifneq ($(OUTPUT_DIR),)

OUTPUT_DIR := $(abspath $(OUTPUT_DIR))

ABS_OBJROOT := $(shell mkdir -p $(OUTPUT_DIR)/objs/ && cd $(OUTPUT_DIR)/objs && pwd)
$(if $(ABS_OBJROOT),, \
     $(error failed to create output directory "$(OUTPUT_DIR)"))

# $(realpath ...) resolves symlinks
ABS_OBJROOT := $(abspath $(ABS_OBJROOT))

THIS := $(lastword $(MAKEFILE_LIST))
ABS_SRCROOT := $(realpath $(dir $(THIS)))

ifneq ($(words $(subst :, ,$(ABS_SRCROOT))), 1)
$(error source directory cannot contain spaces or colons)
endif

CMN_MKFILE := $(abspath CMN.mk)

export CMN_MKFILE
export ABS_OBJROOT
export ABS_SRCROOT
export OUTPUT_DIR

include $(CMN_MKFILE)

SRC_DIR = src
C_FILES = $(SRC_DIR)/*.c
S_FILES = $(SRC_DIR)/*.S
LINKER_SCRIPT = linker.ld

CFLAGS += -Iutils/include
LDFLAGS += -l:libutils.a -L$(OUTPUT_DIR)/

OBJS := $(patsubst $(SRC_DIR)/%.c, $(ABS_OBJROOT)/%_c.o, $(wildcard $(C_FILES)))
OBJS += $(patsubst $(SRC_DIR)/%.S, $(ABS_OBJROOT)/%_s.o, $(wildcard $(S_FILES)))
DEPS := $(patsubst %.o, %.d, $(wildcard $(OBJS)))
UBOOT := $(PREBUILT)/u-boot.bin


all: kernel_ypios.img boot.scr armstub8.bin
.phony: kernel_ypios.img kernel.elf libutils

ifneq ($(DEPS),)
# Adding a prefix '-' before a command line is used to ignore errors.
-include $(DEPS)
endif

$(OUTPUT_DIR)/kernel.elf: $(LINKER_SCRIPT) $(OBJS) libutils
	$(Q)echo recipes of $@ are $^
	$(Q)echo changed deps $?
	$(LD) -T $< $(OBJS) $(LDFLAGS) -o $@

kernel_ypios.img: $(OUTPUT_DIR)/kernel.elf
	$(OBJCOPY) -O binary $< $(OUTPUT_DIR)/kernel.bin
	tools/mkimage -A arm64 -O linux -T kernel -C none -a 0 -e 0 \
	-d $(OUTPUT_DIR)/kernel.bin $(OUTPUT_DIR)/$@
	cp $(OUTPUT_DIR)/$@ $(TFTP_SVR)/

armstub8.bin:
	make -C src/arm -f Makefile

libutils:
	make -C utils/ -f Makefile

boot.scr: $(UBOOT_SRCIPT)
	tools/mkimage -A arm64 -O linux -T script -C none -a 0 -e 0 \
	-n "RAPI4 Boot Script" -d $< $(OUTPUT_DIR)/$@

install: $(PREBUILT) $(BOOTFS_MNT) $(KERNEL_MNT) kernel_ypios.img boot.scr
	-rm $(BOOTFS_MNT)/*
	cp $(PREBUILT)/* $(BOOTFS_MNT)/
	cp $(OUTPUT_DIR)/boot.scr $(BOOTFS_MNT)/
	cp $(OUTPUT_DIR)/armstub8.bin $(BOOTFS_MNT)/
	-rm $(KERNEL_MNT)/*
	cp $(OUTPUT_DIR)/kernel_ypios.img $(KERNEL_MNT)/
	sync

clean:
	rm -rf $(OUTPUT_DIR) *.elf *.img *.scr
	make -C src/arm -f Makefile clean
