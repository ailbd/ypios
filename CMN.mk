#
# Copyright (c) 2023 Jianjun Yue and ypios contributors.
# letscode is licensed under Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
# http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
#


ARMGNU ?= aarch64-linux-gnu
SRC_DIR ?= src
BOOTFS_MNT ?= /media/ailbd/boot
KERNEL_MNT ?= /media/ailbd/kernel
PREBUILT := prebuilt
UBOOT_SRCIPT := rapi4_uboot.script
TFTP_SVR := /mnt/g/tftp-server
CONFIG_MK := Config.mk

include $(ABS_SRCROOT)/$(CONFIG_MK)

CC := $(ARMGNU)-gcc
AS := $(ARMGNU)-as
AR := $(ARMGNU)-ar
CXX := $(ARMGNU)-g++
LD := $(ARMGNU)-ld
OBJCOPY := $(ARMGNU)-objcopy
Q ?= @

CFLAGS := -Iinclude -mgeneral-regs-only -ffreestanding -Wall -march=armv8-a
ASFLAGS :=  -Iinclude  -Wall -march=armv8-a
LDFLAGS := -nostdlib -nostartfiles

ifeq ($(CONFIG_GIC_400),y)
	CFLAGS += -DCONFIG_GIC_400
endif

$(ABS_OBJROOT)/%_s.o: $(SRC_DIR)/%.S
	$(Q)echo $^
# -M(MRI) -MD(dependency tracking)
	$(CC) $(CFLAGS) -MMD -c $< -o $@

$(ABS_OBJROOT)/%_c.o: $(SRC_DIR)/%.c
	$(Q)echo $^
	$(CC) $(CFLAGS) -MMD -c $< -o $@