/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#pragma once

#include "peripheral/base.h"
#include "common.h"

#define DMA_BASE (MP_BASE + 0x02007000) /* 0xFE007000 */

/*
DMA Channel 15 however, is physically removed from the other DMA Channels and so has a different address base of
0x7ee05000. DMA Channel 15 is exclusively used by the VPU
*/
#define DMA_BASE_CHANNEL15  (MP_BASE + 0x02e05000) /* 0xFEE05000 */

#define DMA_MAX_CHANNEL_IDX 15

/*
The BCM2711 DMA Controller provides a total of 16 DMA channels. Four of these are DMA Lite channels (with reduced
performance and features), and four of them are DMA4 channels (with increased performance and a wider address
range).
*/
typedef enum DMA_TYPE {
    TYPE_DMA,
    TYPE_DMA_LITE,
    TYPE_DMA4
} E_DMA_TYPE;

typedef enum DMA_DREQ_TYPE {
    DREQ_DEFAULT = 0,
    DREQ_DSI0_PWM1,
    DREQ_PCM_TX,
    DREQ_PCM_RX,
    DREQ_SMI,
    DREQ_PWM0,
    DREQ_SPI0_TX,
    DREQ_SPI0_RX,
    DREQ_BSC_SPI_SLAVE_TX,
    DREQ_BSC_SPI_SLAVE_RX,
    DREQ_HDMI0,
    DREQ_EMMC,
    DREQ_UART0_TX,
    DREQ_SD_HOST,
    DREQ_UART0_RX,
    DREQ_DSI1,
    DREQ_SPI1_TX,
    DREQ_HDMI1,
    DREQ_SPI1_RX = 18,
    //...
    DREQ_INVALID = 32
} E_DMA_DREQ_TYPE;

/*
struct of TYPE_DMA dma control block, these register cannot directly read.
*/
struct dma_cb_regs {
    reg32 ti;           // transfer information
    reg32 source_ad;    // source address
    reg32 dest_ad;      // destination address
    reg32 txfr_len;     // transfer length
    reg32 stride;       // 2d mode stride
    reg32 nextconbk;    // next control block
    // reg32 __rsv[2];
};

struct dmalite_cb_regs {
    reg32 ti;           // transfer information
    reg32 source_ad;    // source address
    reg32 dest_ad;      // destination address
    reg32 txfr_len;     // transfer length
    reg32 __rsv0;       // * reserved
    reg32 nextconbk;    // next control block
    // reg32 __rsv1[2];
};

struct dma4_cb_regs {
    reg32 ti;           // transfer information
    reg32 scr;          // source address
    reg32 srci;         // * source information
    reg32 dest;         // destination address
    reg32 desti;        // destination info
    reg32 len;          // transfer length
    reg32 next_cb;      // next control block
    // reg32 __rsv[2];
};

struct dma_channel_regs {
    reg32 cs;                           // control and status               0x000
    reg32 conblk_ad;                    // control block address            0x004
    struct dma_cb_regs cb;              // control block register map       0x008 ~ 0x01C
    reg32 debug;                        // debug                            0x020
    reg32 __rsv[(0x100 - 0x020) / 4];
};

struct dmalite_channel_regs {
    reg32 cs;                           // control and status               0x000
    reg32 conblk_ad;                    // control block address            0x004
    struct dmalite_cb_regs cb;          // control block register map       0x008 ~ 0x01C
    reg32 debug;                        // debug                            0x020
    reg32 __rsv[(0x100 - 0x02C) / 4];
};

struct dma4_channel_regs {
    reg32 cs;                           // control and status               0x000
    reg32 cba;                          // control block address            0x004
    reg32 __rsv0;                       // reserved                         0x008
    reg32 debug1;                       // debug                            0x00C
    struct dma4_cb_regs cb;             // control block register map       0x010 ~ 0x028
    reg32 debug2;                       // debug                            0x02C
    reg32 __rsv1[(0x100 - 0x030) / 4];
};

struct dma_regs {
    struct dma_channel_regs dma[7];         // dma channel 0 ~ 6                0x000 ~ 0x6FC
    struct dmalite_channel_regs dmalite[4]; // dma lite channel 7 ~ 10          0x700 ~ 0xAFC
    struct dma4_channel_regs dma4[4];       // dma4 channel 11 ~ 14             0xB00 ~ 0xEFC
    reg32 __rsv0[(0xFE0 - 0xF00) / 4];      // reserved                         0xF00 ~ 0xFDC
    reg32 int_status;                       // Interrupt status of each DMA     0xFE0
    reg32 __rsv1[3];                        //                                  0xFE4 ~ 0xFEC
    reg32 enable;                           // Global enable bits for each DMA  0xFF0
};

#define DMA_REGS ((struct dma_regs *)(DMA_BASE))
#define DMA4_CHAN15_REGS ((struct dma4_channel_regs *)(DMA_BASE_CHANNEL15))