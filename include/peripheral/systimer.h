/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#pragma once

#include "common.h"
#include "peripheral/base.h"

#define SYSTIMER_BASE (MP_BASE + 0x02003000) /* 0xFE003000 */
#define SYSTIMER_CHANNELS 4

struct systimer_regs {
    reg32 cs;
    reg32 clo;
    reg32 chi;
    reg32 channel[4]; /* Channel 0 - 3 */
};

#define SYSTIMER_REGS ((struct systimer_regs *)(SYSTIMER_BASE))
