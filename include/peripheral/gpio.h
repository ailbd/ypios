/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#pragma once

#include "common.h"
#include "peripheral/base.h"

#define GPIO_P_BASE (MP_BASE + 0x02200000) /* 0xFE20 0000 */
#define GPIO_FSEL_1 (GPIO_P_BASE + 0x04) /* 0xFE20 0004 */
#define GPIO_PUPD_0 (GPIO_P_BASE + 0xE4) /* 0xFE20 00E4 */

#define GPIO_FSEL_BITS 30

#if SOC == BCM2711
#define GPIO_MAX_PINID 58
#define GPIO_CONF_ABLE_NUM 46
#else
#define GPIO_MAX_PINID 40
#define GPIO_CONF_ABLE_NUM 40
#endif

typedef enum GPIO_PUP_PDN_STATE {
    GP_NULL = 0,
    GP_PUP = 1,
    GP_PDN = 2,
    GP_RESV = 3,
} GPIO_PP_STATE;

typedef enum gpio_func {
    GPF_INPUT = 0,
    GPF_OUTPUT = 1, // = 1
    GPF_ALT_5 = 2, // = 2
    GPF_ALT_4 = 3, // = 3
    GPF_ALT_0 = 4, // = 4
    GPF_ALT_1 = 5, // = 5,
    GPF_ALT_2 = 6, // = 6,
    GPF_ALT_3 = 7, // = 7
} GPIO_FUNC;

typedef struct gpio_pin_pare {
    reg32 reserved;
    reg32 data[2];
} gpiopinp; /* gpio pin pare */

typedef struct gpio_regs {
    reg32 gpfsel[6]; /* GPFSEL0 ~ GPFSEL5 */
    gpiopinp gpset; /* 0x18: GPSET(GPIO Pin Output Set)0~1 */
    gpiopinp gpclr; /* 0x24: GPCLR(GPIO Pin Output Clear)0~1 */
    gpiopinp gplev; /* 0x30: GPCLR(GPIO Pin Level)0~1 */
    gpiopinp gpeds; /* 0x3c: GPEDS(GPIO Pin Event Detect Status)0~1 */
    gpiopinp gpren; /* 0x48: GPREN(GPIO Pin Rising Edge Detect Enable)0~1 */
    gpiopinp gpfen; /* 0x54: GPFEN(GPIO Pin Falling Edge Detect Enable)0~1 */
    gpiopinp gphen; /* 0x60: GPHEN(GPIO Pin High Detect Enable)0~1 */
    gpiopinp gplen; /* 0x6C: GPLEN(GPIO Pin Low Detect Enable)0~1 */
    gpiopinp gparen; /* 0x78: GPAREN(GPIO Pin Async. Rising Edge Detect)0~1 */
    gpiopinp gpafen; /* 0x84: GPAFEN(GPIO Pin Async. Falling Edge Detect)0~1 */
    reg32 reserved[21]; /* 0x90~0xE4: reserved */
    reg32 gp_pp_clt_regs[4]; /* 0xE4: GPIO Pull-up / Pull-down Register 0~3 */
} gpioregs;

#define GPIO_REGS ((gpioregs *)(GPIO_P_BASE))