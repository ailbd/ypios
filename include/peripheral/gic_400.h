/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#pragma once

#include "common.h"
#include "peripheral/base.h"

/*  gic-400 registers map:

    0x0000-0x0FFF Reserved
    0x1000-0x1FFF Distributor
    0x2000-0x3FFF CPU interfaces
    0x4000-0x4FFF Virtual interface control block, for the processor that is performing the access
    0x5000-0x5FFF Virtual interface control block, for the processor selected by address bits [11:9]
    0x5000-0x51FF
    0x5200-0x53FF
    ...
    0x5E00-0x5FFF
    Alias for Processor 0
    Alias for Processor 1
    ...
    Alias for Processor 7
    0x6000-0x7FFF Virtual CPU interfaces
*/

#define GIC_BASE (LP_BASE + 0x40000) /* 0xFF84 0000 */

typedef struct gic_dist_regs {
    reg32 \
        ctrl,				                    // 0x000
        typer,				                    // 0x004
        iidr,				                    // 0x008
        __rsvd0[(0x080 - 0x00C) / 4],	        // 0x00C ~ 0x07C 
        igroupr[(0x0C0 - 0x080) / 4],	        // 0x080 ~ 0x0BC
        __rsvd1[(0x100 - 0x0C0) / 4],	        // 0x0C0 ~ 0x0FC
        isenabler[(0x140 - 0x100) / 4],			// 0x100 ~ 0x13C
        __rsvd2[(0x180 - 0x140) / 4],	        // 0x140 ~ 0x17C
        icenabler[(0x1C0 - 0x180) / 4],			// 0x180 ~ 0x1BC
        __rsvd3[(0x200 - 0x1C0) / 4],	        // 0x1C0 ~ 0x1FC
        ispendr[(0x240 - 0x200) / 4],			// 0x200 ~ 0x23C
        __rsvd4[(0x280 - 0x240) / 4],	        // 0x240 ~ 0x27C
        icpendr[(0x2C0 - 0x280) / 4],			// 0x280 ~ 0x2BC
        __rsvd5[(0x300 - 0x2C0) / 4],	        // 0x2C0 ~ 0x2FC
        isactiver[(0x340 - 0x300) / 4],			// 0x300 ~ 0x33C
        __rsvd6[(0x380 - 0x340) / 4],	        // 0x340 ~ 0x37C
        icactiver[(0x3C0 - 0x380) / 4],			// 0x380 ~ 0x3BC
        __rsvd7[(0x400 - 0x3C0) / 4],	        // 0x3C0 ~ 0x3FC
        ipriorityr[(0x600 - 0x400) / 4],	    // 0x400 ~ 0x5FC
        __rsvd8[(0x800 - 0x600) / 4],	        // 0x600 ~ 0x7FC
        itargetsr[(0xA00 - 0x800) / 4],			// 0x800 ~ 0x9FC
        __rsvd9[(0xC00 - 0xA00) / 4],	        // 0x8A0 ~ 0xBFC
        icfgr[(0xC80 - 0xC00) / 4],			    // 0xC00 ~ 0xC7C
        __rsvd10[(0xD00 - 0xC80) / 4],	        // 0xC80 ~ 0xCFC
        ppisr,				                    // 0xD00
        spisr[(0xD40 - 0xD04) / 4],			    // 0xD04 ~ 0xD3C
        __rsvd11[(0xF00 - 0xD40) / 4],	        // 0xD40 ~ 0xEFC
        sgir,				                    // 0xF00
        __rsvd12[3],	                        // 0xF04 ~ 0xF0C
        cpendsgir[4],			                // 0xF10 ~ 0xF1C
        spendsgir[4],			                // 0xF20 ~ 0xF2C
        __rsvd13[(0x1000 - 0xF30) / 4];	        // 0xF30 ~ 0xFFC
} gicd_regs;					                // 0x1000 ~ 0x1FFC

typedef struct gic_cpu_ifc_regs {
    volatile uint32_t \
        ctrl,				                    // 0x000
        pmr,				                    // 0x004
        bpr,				                    // 0x008
        iar,				                    // 0x00C
        eoir,				                    // 0x010
        rpr,				                    // 0x014
        hppir,				                    // 0x018
        abpr,				                    // 0x01C
        aiar,				                    // 0x020
        aeoir,				                    // 0x024
        ahippir,			                    // 0x028
        __rsvd0[(0x0D0 - 0x02C) / 4],	        // 0x02C ~ 0x0CC
        apr0,				                    // 0x0D0
        __rsvd1[(0x0E0 - 0x0D4) / 4],	        // 0x0D4 ~ 0x0DC
        nsapr0,				                    // 0x0E0
        __rsvd2[(0x1000 - 0x0E4) / 4],	        // 0x0E4 ~ 0xFFC
        dir;				                    // 0x1000
} gicc_regs;

struct gic_400_regs {
	reg32 __rsv[(0x1000 - 0x0) / 4];	        // 0x000 ~ 0xFFC
    gicd_regs gicd;
    gicc_regs gicc;
    // continue...
};

#if 1

#define GIC_BASE 0xFF840000
#define GICD_DIST_BASE (GIC_BASE+0x00001000)
#define GICC_CPU_BASE (GIC_BASE+0x00002000)

#define GICD_ENABLE_IRQ_BASE (GICD_DIST_BASE+0x00000100)

#define GICC_IAR (GICC_CPU_BASE+0x0000000C)
#define GICC_EOIR (GICC_CPU_BASE+0x00000010)

#define GIC_IRQ_TARGET_BASE (GICD_DIST_BASE+0x00000800)

//VC (=VideoCore) starts at 96
#define SYSTEM_TIMER_IRQ_0 (0x60) //96
#define SYSTEM_TIMER_IRQ_1 (0x61) //97
#define SYSTEM_TIMER_IRQ_2 (0x62) //98
#define SYSTEM_TIMER_IRQ_3 (0x63) //99

#endif

#define GIC_400_REGS ((struct gic_400_regs *)(GIC_BASE))