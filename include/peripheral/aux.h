/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#pragma once

#include "common.h"
#include "peripheral/base.h"

#define AUX_P_BASE (MP_BASE + 0x02215000) /* 0xFC00 0000 + 0x0221 5000 = 0xFE21 5000 */

typedef struct aux_mu_regs { /* aux miniuart registers */
    reg32 aux_mu_io; /* AUX_MU_IO_REG Mini UART I/O Data */
    reg32 aux_mu_ier; /* AUX_MU_IER_REG Mini UART Interrupt Enable */
    reg32 aux_mu_iir; /* AUX_MU_IIR_REG Mini UART Interrupt Identify */
    reg32 aux_mu_lcr; /* AUX_MU_LCR_REG Mini UART Line Control */
    reg32 aux_mu_mcr; /* AUX_MU_MCR_REG Mini UART Modem Control */
    reg32 aux_mu_lsr; /* AUX_MU_LSR_REG Mini UART Line Status */
    reg32 aux_mu_msr; /* AUX_MU_MSR_REG Mini UART Modem Status */
    reg32 aux_mu_scratch; /* AUX_MU_SCRATCH Mini UART Scratch */
    reg32 aux_mu_cntl; /* AUX_MU_CNTL_REG Mini UART Extra Control */
    reg32 aux_mu_stat; /* AUX_MU_STAT_REG Mini UART Extra Status */
    reg32 aux_mu_baud; /* AUX_MU_BAUD_REG Mini UART Baudrate */
    reg32 reserved[5]; /* reserved for miniuart */
} aux_mu_regs;

typedef struct aux_spi_regs {
    reg32 aux_spi_cntl[2];
    reg32 aux_spi_stat;
    reg32 aux_spi_peek;
    reg32 aux_spi_io[4];
    reg32 aux_spi_txhold[4];
} aux_spi_regs;

typedef struct aux_regs {
    reg32 aux_irq;
    reg32 aux_enables;
    reg32 reserved[14]; /* reserved for aux */
    aux_mu_regs mu_regs; /* UART1 */
    aux_spi_regs spi_regs[2]; /* SPI1 & SPI2 */
} auxregs;

#define AUX_REGS ((auxregs *)(AUX_P_BASE))