/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#pragma once

#include "common.h"
#include "peripheral/base.h"

#define ARMC_BASE (MP_BASE + 0x0200B000) /* 0xFE00 B000 */

typedef struct armc_irqx_regs
{
    reg32 irqx_pending[3];                  // 0x00 ~ 0x08
    reg32 __rsv0;                           // 0x0C
    reg32 irqx_set_en[3];                   // 0x10 ~ 0x18
    reg32 __rsv1;                           // 0x1C
    reg32 irqx_clr_en[3];                   // 0x20 ~ 0x28
    reg32 __rsv2;                           // 0x2C
} armc_irqx_regs;

struct armc_regs
{
    reg32 __rsv0[(0x200 - 0x0) / 4];        // 0x000 ~ 0x1FC
    armc_irqx_regs irq0;                    // 0x200 ~ 0x22C
    reg32 irq_status[3];                    // 0x230 ~ 0x238
    reg32 __rsv1;                           // 0x23C
    armc_irqx_regs irq1;                    // 0x240 ~ 0x26C
    reg32 __rsv2[4];
    armc_irqx_regs irq2;                    // 0x280 ~ 0x2AC
    reg32 __rsv3[4];
    armc_irqx_regs irq3;                    // 0x2C0 ~ 0x2EC
    reg32 __rsv4[4];
    armc_irqx_regs fiq0;                    // 0x300 ~ 0x32C
    reg32 __rsv5[4];
    armc_irqx_regs fiq1;                    // 0x340 ~ 0x36C
    reg32 __rsv6[4];
    armc_irqx_regs fiq2;                    // 0x380 ~ 0x3AC
    reg32 __rsv7[4];
    armc_irqx_regs fiq3;                    // 0x3C0 ~ 0x3EC
    reg32 swirq_set;                        // 0x3F0
    reg32 swirq_clr;                        // 0x3F4
};

#define ARMC_REGS ((struct armc_regs *)(ARMC_BASE))