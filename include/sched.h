/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#pragma once

#ifndef __ASSEMBLER__

#include "common.h"
#include "yList.h"
#include "kthread.h"

#define THREAD_SIZE				4096

#define NR_TASKS				64 

#define FIRST_TASK task[0]
#define LAST_TASK task[NR_TASKS-1]

#define TASK_MAX_NAME				32
#define SCHED_TIME_SLICE 1000000

typedef enum SCHED_PRIORITY {
	SCHED_LOW = 0,
	SCHED_MID = 1,
	SCHED_HIGHT = 2,
	SCHED_INVALDk = 3
} SCHED_PRIO_E;

typedef enum TASK_STAT {
	TT_CREATED = 0,
	TT_RUNNING,
	TT_PENDING,
	TT_FINISHED,
	TT_ABORT,
	TT_DESTROYED,
} TASK_STAT;

extern u64 pid_incresed;
extern struct task_struct *current;

struct cpu_context {
	unsigned long x[31];
	unsigned long sp;
	unsigned long elr_el1;
	unsigned long spsr_el1;
	// x30 actually, this is LR. we won't recored this LR.
	// when executing cpu_switch_to, LR must be used
	// to store the address of next instruction immediatlly.
	// because we are interrupt context now.
};

struct task_struct {
	struct ylist_head tasks_list;
	struct ylist_head prio_list;
	struct cpu_context cpu_context;
	struct task_struct *parent;
	char name[TASK_MAX_NAME];
	u16 time_slices;
	pid_t pid;
	u8 state : 3;
	u8 priority : 2;
	u8 preemptible : 1;
	u8 resved : 2;
	u32 attr;
};

extern void sched_init(void);
extern void schedule(void);
extern void timer_tick(void);
extern void preempt_disable(void);
extern void preempt_enable(void);
extern addr_t switch_to(struct task_struct* next);
extern void cpu_switch_to(struct task_struct* prev, struct task_struct* next);
struct task_struct *get_task_by_pid(pid_t pid);
void init_task(struct task_struct *task);
void sched_enqueue(struct task_struct *task);
void prepare_schedual();


#define INIT_TASK() {               	\
    {NULL,NULL},                        \
    {NULL,NULL},                        \
	{0,0,0,0,0,0,0,0,0,0,0,0,0}, 		\
	NULL,								\
	0,0,0,0,0,0,0						\
}

#define DEFINE_TASK(name) \
	static struct task_struct name = INIT_TASK(name);

static int get_pid() {
	return pid_incresed++;
}

#endif

#define CPU_CONTEXT_OFFSET 32