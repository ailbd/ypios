/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#pragma once

#include "common.h"
#include "peripheral/aux.h"
#include "peripheral/gpio.h"

#define GPIO_MU_TXPIN 14 /* choose pin 14 and 15 to serve as miniuart with alt5 */
#define GPIO_MU_RXPIN 15
#define GPIO_MU_FUNC GPF_ALT_5

void muart_init(void);
char muart_recv(void);
void muart_send(char c);
void muart_send_string(char *string);
void muart_handle_irq();
void putc(void *, char);
reg32 muart_stat();