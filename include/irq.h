/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#pragma once

#ifndef __ASSEMBLER__
#ifdef CONFIG_GIC_400
#include "peripheral/gic_400.h"
#else
#include "peripheral/armc.h"
#endif

// video core irq ids; Refs: 6.2.4. VideoCore interrupts
typedef enum VC_IRQ {
    VC_IRQ_TIMER0 = 0,
    VC_IRQ_TIMER1 = 1,
    VC_IRQ_TIMER2 = 2,
    VC_IRQ_TIMER3 = 3,
    // ...
    VC_IRQ_DMA0 = 16,
    VC_IRQ_DMA1 = 17,
    VC_IRQ_DMA2 = 18,
    VC_IRQ_DMA3 = 19,
    VC_IRQ_DMA4 = 20,
    VC_IRQ_DMA5 = 21,
    VC_IRQ_DMA6 = 22,
    VC_IRQ_DMA7_8 = 23,
    VC_IRQ_DMA9_10 = 24,
    VC_IRQ_DMA11 = 25,
    VC_IRQ_DMA12 = 26,
    VC_IRQ_DMA13 = 27,
    VC_IRQ_DMA14 = 28,
    VC_IRQ_AUX = 29,
    VC_IRQ_ARM = 30,
    VC_IRQ_DMA15 = 31,
    VC_IRQ_HDMI_CEC = 32,
    // ...
    VC_IRQ_MAX = 63
} VC_IRQ;

extern addr_t irq_sp;

void irq_init_vectors();
void irq_enable();
void irq_disable();
void handle_irq();
int enable_vc_interrupt(VC_IRQ irq);
int disable_vc_interrupt(VC_IRQ irq);
void irq_int();
#endif
void clean_irq_stack();
