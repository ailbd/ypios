/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#pragma once

#include "common.h"
#include "yList.h"
#include "peripheral/dma.h"

typedef struct dma_ops {
    void (*enable)(u8);
    void (*disable)(u8);
    int (*request)(u8);
    void (*reset)(u8);
} dma_ops_t;

struct dma_control_block {
    u32 trans_info;             // transfer information
    addr_t source_adr;          // source address
    u32 source_info;            // source information
    addr_t dest_adr;            // destination address
    u32 dest_info;
    u32 trans_len;              // transfer length
    u32 stride;
    struct dma_control_block *next;
    dma_ops_t dma_ops;
};

struct dma {
    struct ylist_head list;
    u8 dma_channel;
    E_DMA_DREQ_TYPE dreq;
    struct dma_control_block *dma_cb;               // dma control blcok
};
