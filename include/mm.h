/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#pragma once

#define PAGE_SHIFT 12
#define TABLE_SHIFT 9
#define SECTION_SHIFT (TABLE_SHIFT + PAGE_SHIFT)

#define PAGE_SIZE (1 << PAGE_SHIFT) // 2^12 = 2^2 * 2^10 (1k) = 4096 (4k)
#define SECTION_SIZE (1 << SECTION_SHIFT) // 2^9 * 2^12 = 512 * 4096

#define LOW_MEMORY (2 * SECTION_SIZE) // 4M

#ifndef __ASSEMBLER__
#include "peripheral/base.h"

#define HIGH_MEMORY MP_BASE
#define PAGING_MEMORY (HIGH_MEMORY - LOW_MEMORY)
#define PAGING_PAGES (PAGING_MEMORY / PAGE_SIZE)

#define PAGE_ALIGN_HIGH(adr) ((adr / PAGE_SIZE + 1) * (PAGE_SIZE))
#define PAGE_ALIGN_LOW(adr) ((adr / PAGE_SIZE) * (PAGE_SIZE))

#include "common.h"
addr_t get_free_page();
void free_page(addr_t p);
void memzero(addr_t src, u64 lengh);
void memread(addr_t src, addr_t dst, u32 size);
void ymemcpy(addr_t dst, addr_t src, u64 size);
void examine_bitmap(u32 index);

/**
 * @brief examine memory at @param start with @param size byte len.
 * 
 * @param start start address
 * @param size memory size in bytes
 */
void examine_mem(addr_t start, u32 size);
#endif

#ifdef __ASSEMBLER__
.macro print_greg reg
    sub sp, sp, #8
    str x0, [sp]
    mov x0, \reg
    bl examine_register
    ldr x0, [sp]
    add sp, sp, #8
.endm

.macro print_sreg sreg
    sub sp, sp, #8
    str x0, [sp]
    mrs x0, \sreg
    bl examine_register
    ldr x0, [sp]
    add sp, sp, #8
.endm

.macro print_lable label
    sub sp, sp, #8
    str x0, [sp]
    adr x0, \label
    bl examine_register
    ldr x0, [sp]
    add sp, sp, #8
.endm
#endif