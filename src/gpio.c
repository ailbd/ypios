/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "gpio.h"

void gppin_set_func(u8 pinid, GPIO_FUNC func)
{
    u8 gpfsel_id = 0;
    u8 start_bit = 0;
    reg32 selector = INVALID_REG32;

    if (pinid >= GPIO_MAX_PINID || func > GPF_ALT_3)
        return;

    gpfsel_id = pinid / 10; // each function-select register controls 10 pins
    start_bit = (pinid % 10) * 3; // get the order index by devide 10.

    selector = GPIO_REGS->gpfsel[gpfsel_id];

    selector &= (~(7 << start_bit)); // clear 3 bits begin at start_bit
    selector |= (func << start_bit);

    GPIO_REGS->gpfsel[gpfsel_id] = selector;
}

void gppin_pp_enable(u8 pinid, GPIO_PP_STATE pp)
{
    u8 gpio_pp_regid = 0;
    u8 start_bit = 0;
    reg32 selector = INVALID_REG32;

    if (pinid >= GPIO_MAX_PINID || pp > GP_RESV)
        return;

    gpio_pp_regid = pinid / 16; /* each GPIO_PUP_PDN_CNTRL_REG control 16 pins */
    start_bit = (pinid % 16) * 2;

    selector = GPIO_REGS->gp_pp_clt_regs[gpio_pp_regid];
    selector &= (~(3 << start_bit)); // clear 2 bits begin at start_bit
    selector |= (pp << start_bit);

    GPIO_REGS->gp_pp_clt_regs[gpio_pp_regid] = selector;
}

