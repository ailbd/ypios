/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "common.h"
#include "dma.h"
#include "log.h"
#include "peripheral/dma.h"
#include "irq.h"
#include "yList.h"

YLIST_HEAD(dma_list);

static struct dma_ops dmaops = {
    .enable = NULL,
    .disable = NULL,
    .request = NULL,
    .reset = NULL
};

void dma_channel_reset(u8 channel) {
    if (channel > DMA_MAX_CHANNEL_IDX) {
        YLOG_E("Invalid channel id %d\n", channel);
        return;
    }

    if (channel < 7) {
        DMA_REGS->dma[channel].cb.nextconbk = 0;    // set next block addr to invalid
        DMA_REGS->dma[channel].cs |= BIT(30);       // bit 30 : abort current DMA CB
        DMA_REGS->dma[channel].cs |= BIT(31);       // bit 31 : reset DMA channel
    } else if (channel < 11) {
        DMA_REGS->dmalite[channel - 7].cb.nextconbk = 0;
        DMA_REGS->dmalite[channel - 7].cs |= BIT(30);
        DMA_REGS->dmalite[channel - 7].cs |= BIT(31);
    } else if (channel < 15) {
        DMA_REGS->dma4[channel - 11].cb.next_cb = 0;
        DMA_REGS->dma4[channel - 11].cs |= BIT(30);
        DMA_REGS->dma4[channel - 11].cs |= BIT(31);
    }
}

void dma_enable_channel(u8 channel) {
    if (channel > DMA_MAX_CHANNEL_IDX) {
        YLOG_E("Invalid channel id %d\n", channel);
        return;
    }

    if (channel < 15) {
        DMA_REGS->enable |= BIT(channel);
    }
}

void dma_disable_channel(u8 channel) {
    if (channel > DMA_MAX_CHANNEL_IDX) {
        YLOG_E("Invalid channel id %d\n", channel);
        return;
    }

    if (channel < 15) {
        DMA_REGS->enable &= ~BIT(channel);
    }
}

void dma_data_requst(u8 channel) {
    if (channel > DMA_MAX_CHANNEL_IDX) {
        YLOG_E("Invalid channel id %d\n", channel);
        return;
    }

    if (channel < 7) {
        DMA_REGS->dma[channel].cs |= BIT(0);
    } else if (channel < 11) {
        DMA_REGS->dmalite[channel - 7].cs |= BIT(0);
    } else if (channel < 15) {
        DMA_REGS->dma4[channel - 11].cs |= BIT(0);
    } else {
        DMA4_CHAN15_REGS->cs |= BIT(0);
    }
}

void dma_init() {
    YLOG_T("dma init...\n");

    for (int i = 0; i < DMA_MAX_CHANNEL_IDX; i++) {
        dma_disable_channel(i);
    }

    dmaops.disable = &dma_disable_channel;
    dmaops.enable = &dma_enable_channel;
    dmaops.request = &dma_data_requst;
    dmaops.reset = &dma_channel_reset;

    enable_vc_interrupt(VC_IRQ_DMA0);
    enable_vc_interrupt(VC_IRQ_DMA1);
    enable_vc_interrupt(VC_IRQ_DMA2);
    enable_vc_interrupt(VC_IRQ_DMA3);
    enable_vc_interrupt(VC_IRQ_DMA4);
    enable_vc_interrupt(VC_IRQ_DMA5);
    enable_vc_interrupt(VC_IRQ_DMA6);
    enable_vc_interrupt(VC_IRQ_DMA7_8);
    enable_vc_interrupt(VC_IRQ_DMA9_10);
    enable_vc_interrupt(VC_IRQ_DMA11);
    enable_vc_interrupt(VC_IRQ_DMA12);
    enable_vc_interrupt(VC_IRQ_DMA13);
    enable_vc_interrupt(VC_IRQ_DMA14);
    enable_vc_interrupt(VC_IRQ_DMA15);

    YLOG_T("dma init...OK\n");
}
