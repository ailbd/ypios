/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "common.h"
#include "peripheral/systimer.h"
#include "systimer.h"
#include "log.h"
#include "mini_uart.h"
#include "irq.h"
#include "sched.h"

#define TIMER0_INTERVAL 10000        // 6 ms
#define TIMER1_INTERVAL 2000000
#define TIMER2_INTERVAL 4000000
#define TIMER3_INTERVAL 5000000

u32 systimer_get_ticks_low32() {
    u32 low = SYSTIMER_REGS->clo;
    u32 high = SYSTIMER_REGS->chi;

    if (high != SYSTIMER_REGS->chi)
        low = SYSTIMER_REGS->clo;

    return low;
}

void systimer_init() {
    SYSTIMER_REGS->cs = 10;
    u32 cur_count = systimer_get_ticks_low32();

    SYSTIMER_REGS->channel[1] = cur_count + TIMER1_INTERVAL;
    SYSTIMER_REGS->channel[3] = cur_count + TIMER3_INTERVAL;

    // Do not enable VC_IRQ_TIMER0 irq routting. this will cause irq flood.
    // enable_vc_interrupt(VC_IRQ_TIMER0);
    enable_vc_interrupt(VC_IRQ_TIMER1);
    // enable_vc_interrupt(VC_IRQ_TIMER2);
    enable_vc_interrupt(VC_IRQ_TIMER3);
}

/*
    timer0 and timer2 is used by video core internally. so we cannot use it.
*/
void handle_systimer_irq() {
    u8 idx = 0;
    reg32 mstat = 0;
    reg32 irq_status = SYSTIMER_REGS->cs;

    do {
        if (SYSTIMER_REGS->cs & BIT(idx)) {
            mstat = muart_stat();

            YLOG_T("STimer%u, mstat:0x%x\n", idx, mstat);

            SYSTIMER_REGS->channel[idx] += TIMER0_INTERVAL;
            SYSTIMER_REGS->cs |= BIT(idx);  // clear channel idx irq
        }
    } while (idx++ < SYSTIMER_CHANNELS);
}

u32 get_systimer_channel(u8 idx) {
    if (idx > 3) {
        return 0;
    }

    return SYSTIMER_REGS->channel[idx];
}

void set_systimer(u8 timer, u32 ticks) {
    u32 cur = systimer_get_ticks_low32();
    YLOG_T("curticks=%X\n", cur);

    if (timer > 3) {
        return 0;
    }

    SYSTIMER_REGS->cs &= ~(BIT(timer));
    SYSTIMER_REGS->channel[timer] = ticks + cur;
    YLOG_T("setticks=%X\n", ticks + cur);
}

/**
 * @brief this timer dedicatly used by schedular.
 * 
 */
void handle_systimer1_irq() {
    if (SYSTIMER_REGS->cs & BIT(1)) {

        YLOG_W("STimer1 start scheduling...\n");

        schedule();

        set_systimer(1, TIMER1_INTERVAL);
        SYSTIMER_REGS->cs |= BIT(1);  // clear channel idx irq
    }
}

void handle_systimer3_irq() {
    reg32 mstat = 0;

    if (SYSTIMER_REGS->cs & BIT(3)) {
        YLOG_W("STimer3\n");

        SYSTIMER_REGS->channel[3] += TIMER3_INTERVAL;
        SYSTIMER_REGS->cs |= BIT(3);  // clear channel idx irq
    }
}

u64 systimer_get_ticks() {
    u32 low = SYSTIMER_REGS->clo;
    u32 high = SYSTIMER_REGS->chi;

    return ((high << 32) | low);
}

void systimer_sleep(u32 milisec) {
    u64 start = systimer_get_ticks();

    while(systimer_get_ticks() < start + (milisec * 1000));
}
