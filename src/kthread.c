/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "kthread.h"
#include "log.h"
#include "mm.h"
#include "sched.h"
#include "printf.h"
#include "fork.h"
#include "systimer.h"

int kthread_create(kthread_t *kt, kthread_fn func, char *name, void *args) {
    if (kt == NULL | func == NULL) {
        YLOG_E("create kernel thread failed");
        return -1;
    }

    struct task_struct *task = get_free_page();
    YLOG_D("create task at %x\n", task);
    init_task(task);

    task->cpu_context.x[19] = args;
    task->cpu_context.elr_el1 = (unsigned long)func;
    task->cpu_context.spsr_el1 = 0;
    task->cpu_context.sp = task + THREAD_SIZE;
    task->priority = SCHED_LOW;

    YLIST_NODE_INIT(task->prio_list);
    YLIST_NODE_INIT(task->tasks_list);
    sprintf(task->name, "%s", name);

    sched_enqueue(task);

    *kt = task;

    return 0;
}

#define TASK_ATTR_EXIT_FLAG BIT(2)
void *kthread_test_func1(void *arg) {
    u32 counter = 0;
    u64 time_ticks = 0;

    while (!(current->attr & TASK_ATTR_EXIT_FLAG)) {
        counter++;

        if (!(counter % 100000)) {
            printf("thread %s heartbeats\n", current->name);
            time_ticks = systimer_get_ticks();
            
            printf("timer_ticks:%X, timer1:%X\n", time_ticks,
                get_systimer_channel(1));
            printf("timer_ticks:%X, timer3:%X\n", time_ticks,
                get_systimer_channel(3));
        }
    }
}

void *kthread_test_func2(void *arg) {
    u32 counter = 0;

    while (!(current->attr & TASK_ATTR_EXIT_FLAG)) {
        counter++;

        if (!(counter % 100000)) {
            printf("thread %s heartbeats\n", current->name);
        }
    }
}