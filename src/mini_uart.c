/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "mini_uart.h"
#include "gpio.h"
#include "irq.h"
#include "log.h"

void muart_init(void) {
    gppin_set_func(GPIO_MU_TXPIN, GPIO_MU_FUNC);
    gppin_set_func(GPIO_MU_RXPIN, GPIO_MU_FUNC);

    gppin_pp_enable(GPIO_MU_TXPIN, GP_NULL);
    gppin_pp_enable(GPIO_MU_RXPIN, GP_NULL);

    AUX_REGS->aux_enables = 1; /* to assess aux peripherals */
    AUX_REGS->mu_regs.aux_mu_cntl = 0; /* disable auto flow control and TX & RX */
    AUX_REGS->mu_regs.aux_mu_baud = 541; /* 115200bps @ 500MHZ*/
    AUX_REGS->mu_regs.aux_mu_ier = 2; /* generates rx interrupts */
    AUX_REGS->mu_regs.aux_mu_lcr = 3; /* set miniuart works in 8-bits mode */
    AUX_REGS->mu_regs.aux_mu_mcr = 0;
    AUX_REGS->mu_regs.aux_mu_cntl = 3; /* to enable tx and rx */

    // aux will cause irq flood. even if aux_irq = 0
    // enable_vc_interrupt(VC_IRQ_AUX);

    muart_send('\r');
    muart_send('\n');
    muart_send('\n');
}

void muart_send(char c) {
    while (!(AUX_REGS->mu_regs.aux_mu_lsr & 0x20)) {}; /* 0x20: 0010 0000 Transmitter empty bit */    

    AUX_REGS->mu_regs.aux_mu_io = c;
}

char muart_recv(void) {
    if (!(AUX_REGS->mu_regs.aux_mu_lsr & 1))
        return '\0';

    return AUX_REGS->mu_regs.aux_mu_io & 0xFF;
}

void muart_send_string(char *string) {
    while (*string) {
        if (*string == '\n') {
            muart_send('\r');
        }

        muart_send(*string);
        string++;
    }
}

void putc(void *ptr, char c) {
    unused(ptr);

    if (c == '\n') {
        muart_send('\r');
    }

    muart_send(c);
}

reg32 muart_stat() {
    return AUX_REGS->mu_regs.aux_mu_stat;
}

void muart_handle_irq() {
    printf("aux.mu.iir=%X\n", AUX_REGS->mu_regs.aux_mu_iir);
    printf("aux.irq=%X\n", AUX_REGS->aux_irq);

    if ((AUX_REGS->mu_regs.aux_mu_iir & 6) == 4) {
        printf("UART Recv: ");
        printf("%c\r\n", muart_recv());
        // muart_send("\n"); // can not use muart_send, this will disturb output
    }

    // clear recive fifo to clear irq
    AUX_REGS->mu_regs.aux_mu_iir = 0x2;
}