/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "mm.h"
#include "common.h"
#include "log.h"

static u64 bitmap[PAGING_PAGES / 64 + 1] = {0};

addr_t get_free_page()
{
	int i = 0;

	while ((i < PAGING_PAGES / 64 + 1) &&
		   (bitmap[i / 64] & BIT(i % 64))) {
		i++;
	}

	if (i == PAGING_PAGES / 64 + 1)
		return 0;

	bitmap[i / 64] |= BIT(i % 64);

	return (LOW_MEMORY + i * PAGE_SIZE);
}

void free_page(addr_t adrs){
	addr_t aligned = PAGE_ALIGN_LOW(adrs);

	int idx = (aligned - LOW_MEMORY) / PAGE_SIZE;

	memzero(aligned, PAGE_SIZE);

	bitmap[idx / 64] &= ~BIT(idx % 64);
}

void examine_bitmap(u32 index) {
	if (index > PAGING_PAGES / 64) {
		YLOG_E("index invalid.\n");
	}

	printf("bitmap[%d]_l:%x\n", index, bitmap[index] & 0xFFFFFFFF);
	printf("bitmap[%d]_h:%x\n", index, bitmap[index] >> 32);
}

#define MAX_BUFFER_LENGTH 4096
#define LINE_BYTES 4	// 8
void examine_mem(addr_t start, u32 size) {
	if (size > MAX_BUFFER_LENGTH) {
		YLOG_D("buffer size overflow.\n");
		size = MAX_BUFFER_LENGTH;
	}

	u32 lines = 0;
	u8 buffer[MAX_BUFFER_LENGTH];

	ymemcpy(buffer, start, size);

	if (size % LINE_BYTES) {
		lines = size / LINE_BYTES;
	} else {
		lines = size / LINE_BYTES + 1;
	}

	printf("Memory begin at: %x\n", start);
	for (int i = 0; i < lines; i++) {
		printf("0x%X:", start + 4 * i);

		for (int j = 0; j < LINE_BYTES; j++) {
			printf("%2X", buffer[i * LINE_BYTES + j]);
			if (((j + 1) % 4) && j != LINE_BYTES - 1) {
				printf(" ");
			}
		}

		printf("\n");
	}
}

void examine_register(u64 value) {
	printf("reg value:%X\n", value);
}