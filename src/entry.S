/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "mm.h"
#include "entry.h"
#include "sched.h"

// The stp instruction in ARM assembly is used to store a pair of consecutive registers to memory.
// It is commonly used to store the contents of two consecutive registers (paired registers) in
// memory, providing a way to save and restore register values efficiently.

// In ARMv8-A architecture, the general-purpose registers (GPRs) are 64 bits wide. Each general-purpose
// register is capable of holding a 64-bit integer or address.

.macro kernel_entry
	# reserve original x8
    sub sp, sp, #8
	str x8, [sp]
	# read irq_sp address into x8
	adr x8, irq_sp
	ldr x8, [x8]
	print_greg x8

    stp x0,  x1,  [x8, #16 * 0 ]
    stp x2,  x3,  [x8, #16 * 1 ]
    stp	x4,  x5,  [x8, #16 * 2 ]
	stp	x6,  x7,  [x8, #16 * 3 ]
	stp	x10, x11, [x8, #16 * 5 ]
	stp	x12, x13, [x8, #16 * 6 ]
	stp	x14, x15, [x8, #16 * 7 ]
	stp	x16, x17, [x8, #16 * 8 ]
	stp	x18, x19, [x8, #16 * 9 ]
	stp	x20, x21, [x8, #16 * 10]
	stp	x22, x23, [x8, #16 * 11]
	stp	x24, x25, [x8, #16 * 12]
	stp	x26, x27, [x8, #16 * 13]
	stp	x28, x29, [x8, #16 * 14]
	# copy irq_sp address to x10
	mov x10, x8
	mov x11, sp
	mrs x12, elr_el1
	mrs x13, spsr_el1
	# resotre original x8 and restore sp
	ldr x8, [sp]
	add sp, sp, #8
	# store original x8, x9 into cpu_context
	stp	x8,  x9,  [x10, #16 * 4]
	stp x30, x11, [x10, #16 * 15]
	stp x12, x13, [x10, #16 * 16]

.endm

.macro kernel_exit
	# read current address into x8
	adr x8, irq_sp
	ldr x8, [x8]
	print_greg x8

	# restore x30 and sp
	ldp	x30, x9,  [x8, #16 * 15]
	mov sp, x9
	# print_greg sp
	# restore elr_el1 and spsr_el1
	ldp x10, x11, [x8, #16 * 16]
	msr elr_el1, x10
	# print_sreg elr_el1
	msr spsr_el1, x11
	# print_sreg spsr_el1
	# print_lable kthread_test_func

    ldp x0,  x1,  [x8, #16 * 0]
    ldp x2,  x3,  [x8, #16 * 1]
    ldp	x4,  x5,  [x8, #16 * 2]
	ldp	x6,  x7,  [x8, #16 * 3]
	
	ldp	x10, x11, [x8, #16 * 5]
	ldp	x12, x13, [x8, #16 * 6]
	ldp	x14, x15, [x8, #16 * 7]
	ldp	x16, x17, [x8, #16 * 8]
	ldp	x18, x19, [x8, #16 * 9]
	ldp	x20, x21, [x8, #16 * 10]
	ldp	x22, x23, [x8, #16 * 11]
	ldp	x24, x25, [x8, #16 * 12]
	ldp	x26, x27, [x8, #16 * 13]
	ldp	x28, x29, [x8, #16 * 14]
	# reserve address of current->cpu_context
    sub sp, sp, #8
	str x8, [sp]
	# restore x8, x9 from sp
	ldp	x8,  x9,  [sp, #16 * 4]

	add sp, sp, #8
	bl clean_irq_stack
    eret
.endm

.macro handle_invalid_entry type
	kernel_entry
	mov	x0, #\type
	mrs	x1, esr_el1
	mrs	x2, elr_el1
	bl	show_invalid_entry_message
	b	err_hang
.endm

.macro	ventry	label
.align	7
    b	\label
.endm

// D1.10.2 Exception vectors table
// why set this alignment 11 to achive 0x80 alignment?
// another question, why target exception level is el1? why not el2?
.align	11
.globl vectors 
vectors:
	ventry	sync_invalid_el1t			// Synchronous EL1t
	ventry	irq_invalid_el1t			// IRQ EL1t
	ventry	fiq_invalid_el1t			// FIQ EL1t
	ventry	error_invalid_el1t			// Error EL1t

	ventry	sync_invalid_el1h			// Synchronous EL1h
	ventry	handle_el1_irq				// IRQ EL1h
	ventry	fiq_invalid_el1h			// FIQ EL1h
	ventry	error_invalid_el1h			// Error EL1h

	ventry	sync_invalid_el0_64			// Synchronous 64-bit EL0
	ventry	irq_invalid_el0_64			// IRQ 64-bit EL0
	ventry	fiq_invalid_el0_64			// FIQ 64-bit EL0
	ventry	error_invalid_el0_64		// Error 64-bit EL0

	ventry	sync_invalid_el0_32			// Synchronous 32-bit EL0
	ventry	irq_invalid_el0_32			// IRQ 32-bit EL0
	ventry	fiq_invalid_el0_32			// FIQ 32-bit EL0
	ventry	error_invalid_el0_32		// Error 32-bit EL0


sync_invalid_el1t:
	handle_invalid_entry  SYNC_INVALID_EL1t

irq_invalid_el1t:
	handle_invalid_entry  IRQ_INVALID_EL1t

fiq_invalid_el1t:
	handle_invalid_entry  FIQ_INVALID_EL1t

error_invalid_el1t:
	handle_invalid_entry  ERROR_INVALID_EL1t

sync_invalid_el1h:
	handle_invalid_entry  SYNC_INVALID_EL1h

fiq_invalid_el1h:
	handle_invalid_entry  FIQ_INVALID_EL1h

error_invalid_el1h:
	handle_invalid_entry  ERROR_INVALID_EL1h

sync_invalid_el0_64:
	handle_invalid_entry  SYNC_INVALID_EL0_64

irq_invalid_el0_64:
	handle_invalid_entry  IRQ_INVALID_EL0_64

fiq_invalid_el0_64:
	handle_invalid_entry  FIQ_INVALID_EL0_64

error_invalid_el0_64:
	handle_invalid_entry  ERROR_INVALID_EL0_64

sync_invalid_el0_32:
	handle_invalid_entry  SYNC_INVALID_EL0_32

irq_invalid_el0_32:
	handle_invalid_entry  IRQ_INVALID_EL0_32

fiq_invalid_el0_32:
	handle_invalid_entry  FIQ_INVALID_EL0_32

error_invalid_el0_32:
	handle_invalid_entry  ERROR_INVALID_EL0_32

handle_el1_irq:
	kernel_entry
	bl	handle_irq
	kernel_exit

.globl err_hang
err_hang: b err_hang

