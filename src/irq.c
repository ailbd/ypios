/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "common.h"
#include "peripheral/aux.h"
#include "irq.h"
#include "printf.h"
#include "log.h"
#include "systimer.h"
#include "mini_uart.h"
#include "mm.h"

static u32 irq_count = 0, irq1_cnt = 0, irq2_cnt = 0;
/* interrupt stack pointer  */
addr_t irq_sp = 0;


const char entry_error_messages[16][32] = {
    "SYNC_INVALID_EL1t",
    "IRQ_INVALID_EL1t",		
    "FIQ_INVALID_EL1t",		
    "ERROR_INVALID_EL1T",		

    "SYNC_INVALID_EL1h",
    "IRQ_INVALID_EL1h",		
    "FIQ_INVALID_EL1h",		
    "ERROR_INVALID_EL1h",		

    "SYNC_INVALID_EL0_64",		
    "IRQ_INVALID_EL0_64",		
    "FIQ_INVALID_EL0_64",		
    "ERROR_INVALID_EL0_64",	

    "SYNC_INVALID_EL0_32",		
    "IRQ_INVALID_EL0_32",		
    "FIQ_INVALID_EL0_32",		
    "ERROR_INVALID_EL0_32"	
};

/*
 * Because we only use processor 0 by 'proc_hang' functionality within boot.S. 
 * here we just enable corresponding IRQ of ARM Core 0.
 */
#if 1
#if 1
// #ifdef CONFIG_GIC_400
#include "peripheral/gic_400.h"

int enable_vc_interrupt(VC_IRQ irq) {
    if (irq > VC_IRQ_MAX) {
        return -1;
    }

    irq += 96;  // vc irq maps to spi irq 96~159
    GIC_400_REGS->gicd.isenabler[irq / 32] |= BIT(irq % 32);

    return 0;
}

int disable_vc_interrupt(VC_IRQ irq) {
    if (irq > VC_IRQ_MAX) {
        return -1;
    }

    irq += 96;  // vc irq maps to spi irq 96~159
    GIC_400_REGS->gicd.icenabler[irq / 32] |= BIT(irq % 32);

    return 0;
}

/**
 * gic 400 distributor initilize
 * 
*/
static void gic_dst_init()
{
    u32 gic_irqs;
    u32 cpumask;
    int i;

    GIC_400_REGS->gicd.ctrl = 0;

    /*
     * Find out how many interrupts are supported.
     * The GIC only supports up to 1020 interrupt sources.
     */
    gic_irqs = GIC_400_REGS->gicd.typer & 0x1F;
    gic_irqs = (gic_irqs + 1) * 32;
    if(gic_irqs > 1020) {
        gic_irqs = 1020;
    }

    /*
     * Set all global interrupts to this CPU 0.
     */
    cpumask = 0x1;
    cpumask |= cpumask << 8;
    cpumask |= cpumask << 16;
    // GICD_ITARGETSR[0~7] Read Only
    for(i = 32; i < gic_irqs; i += 4) {
        GIC_400_REGS->gicd.itargetsr[i / 4] = cpumask;
    }

    /*
     * Set all global interrupts to be level triggered, active low.
     */
    for(i = 32; i < gic_irqs; i += 16) {
        GIC_400_REGS->gicd.icfgr[i / 16] = 0;
    }

    /*
     * Set priority on all global interrupts.
     */
    // for(i = 32; i < gic_irqs; i += 4) {
    //     GIC_400_REGS->gicd.ipriorityr[i / 4] = 0xA0A0A0A0;
    // }

    /*
     * Disable all interrupts, leave the SGI and PPI alone
     * as these enables are banked registers.
     */
    for(i = 32; i < gic_irqs; i += 32) {
        GIC_400_REGS->gicd.icenabler[i / 32] = 0xFFFFFFFF;
    }

    // enable group 1 non-secure
    GIC_400_REGS->gicd.ctrl |= BIT(1);
    // disable secure
    GIC_400_REGS->gicd.ctrl |= BIT(6);
}

static void gic_cpuif_init()
{
    int i;

    /*
     * Deal with the banked SGI and PPI interrupts - enable all
     * SGI interrupts, ensure all PPI interrupts are disabled.
     */
    GIC_400_REGS->gicd.icenabler[0] = 0xFFFF0000;
    GIC_400_REGS->gicd.isenabler[0] = 0x0000FFFF;

    /*
     * Set priority on SGI and PPI interrupts
     */
    // for(i = 0; i < 32; i += 4) {
    //     GIC_400_REGS->gicd.ipriorityr[i / 4] = 0xA0A0A0A0;
    // }

    GIC_400_REGS->gicc.pmr = 0xF0;
    GIC_400_REGS->gicc.ctrl = 0x1;
}

void gic_init() {
    YLOG_T("gic-400 initializing...\n");
    gic_dst_init();
    gic_cpuif_init();
}

void handle_irq() {
    u32 irq = 0;
    u32 irq_count = 0;

    irq = GIC_400_REGS->gicc.iar &= 0x3FF;
    YLOG_T("irq = %X\n", irq);

    if(0) {

        if (irq & BIT(VC_IRQ_AUX)) {
            irq &= ~BIT(VC_IRQ_AUX);

            muart_handle_irq();
        }

        if (irq & BIT(VC_IRQ_TIMER1)) {
            irq &= ~BIT(VC_IRQ_TIMER1);
            handle_systimer1_irq();
        }

        if (irq & BIT(VC_IRQ_TIMER3)) {
            irq &= ~BIT(VC_IRQ_TIMER3);
            handle_systimer3_irq();
        }
    }

    // Indicates to the Distributor that it can forward the
    // new highest priority pending interrupt
    GIC_400_REGS->gicc.eoir = irq;

    YLOG_T("irq_count: %d", irq_count);
}

#else
#include "peripheral/armc.h"

int enable_vc_interrupt(VC_IRQ irq) {
    if (irq > VC_IRQ_MAX) {
        return -1;
    }

    if (irq < VC_IRQ_HDMI_CEC) {
        ARMC_REGS->irq0.irqx_set_en[0] = BIT(irq);
    } else {
        ARMC_REGS->irq0.irqx_set_en[1] = BIT(irq % 32);
    }

    return 0;
}

int disable_vc_interrupt(VC_IRQ irq) {
    if (irq > VC_IRQ_MAX) {
        return -1;
    }

    if (irq < VC_IRQ_HDMI_CEC) {
        ARMC_REGS->irq0.irqx_clr_en[0] = BIT(irq);
    } else {
        ARMC_REGS->irq0.irqx_clr_en[1] = BIT(irq % 32);
    }

    return 0;
}

void handle_irq() {
    u32 irq0, irq1, irq2;

    irq0 = ARMC_REGS->irq0.irqx_pending[0];
    irq1 = ARMC_REGS->irq0.irqx_pending[1];
    irq2 = ARMC_REGS->irq0.irqx_pending[2];

    // YLOG_D("irq0: %x, irq1: %x, irq2: %x\n", irq0, irq1, irq2);
    printf("armc irq_status0=%X\n", ARMC_REGS->irq_status[0]);
    irq_count++;

    if(irq0) {
        YLOG_T("irq0 = %X\n", irq0);
        irq_count++;

        if (irq0 & BIT(VC_IRQ_AUX)) {
            YLOG_D("irq_count: %d, irq0:%x\n", irq_count, irq0);
            irq0 &= ~BIT(VC_IRQ_AUX);

            muart_handle_irq();
        }

        if (irq0 & BIT(VC_IRQ_TIMER1)) {
            irq0 &= ~BIT(VC_IRQ_TIMER1);
            handle_systimer1_irq();
        }

        if (irq0 & BIT(VC_IRQ_TIMER3)) {
            irq0 &= ~BIT(VC_IRQ_TIMER3);
            handle_systimer3_irq();
        }
    }

    if (irq1) {
        irq1_cnt++;
    }

    if (irq2) {
        irq2_cnt++;
    }

    YLOG_T("irq_count: %d, irq1_cnt=%d, irq2_cnt=%d\n", irq_count,
        irq1_cnt, irq2_cnt);
}

#endif
#else
#include "peripheral/gic_400.h"
#include "arm/sysregs.h"
#include "utils.h"

int enable_vc_interrupt(VC_IRQ irq) {
    irq += 96;

    enable_interrupt(irq);
    assign_target(irq, 0);
}

void enable_interrupt(unsigned int irq) {
    printf("%x\r\n", irq);
    unsigned int n = irq / 32;
    unsigned int offset = irq % 32;
    unsigned int enableRegister = GICD_ENABLE_IRQ_BASE + (4*n);
    printf("EnableRegister: %x\r\n", enableRegister);
    put32(enableRegister, 1 << offset);
}

void assign_target(unsigned int irq, unsigned int cpu) {
    unsigned int n = irq / 4;
    unsigned int offset_8 = irq % 4;
    unsigned int targetRegister = GIC_IRQ_TARGET_BASE + (4*n);
    // Currently we only enter the target CPU 0
    put32(targetRegister, get32(targetRegister) | BIT(8 * offset_8));
}

void enable_interrupt_controller() {	
    assign_target(SYSTEM_TIMER_IRQ_1, 0);
    enable_interrupt(SYSTEM_TIMER_IRQ_1);
}

void handle_irq(void) {
    unsigned int irq_ack_reg = get32(GICC_IAR);
    unsigned int irq = irq_ack_reg & 0x2FF;
    switch (irq) {
        case (SYSTEM_TIMER_IRQ_1):
        printf("systimer 1 intterrupted\r\n");
        put32(GICC_EOIR, irq_ack_reg);
        break;
        default:
        printf("Unknown pending irq: %x\r\n", irq);
        break;
    }
}
#endif

void show_invalid_entry_message(u32 type, u64 esr, u64 address) {
    printf("ERROR CAUGHT: %s - %d, ESR: %X, Address: %X\n", 
        entry_error_messages[type], type, esr, address);
}

void clean_irq_stack() {
    if (!irq_sp)
        return;

    memzero(irq_sp, PAGE_SIZE);
}

void irq_init() {
    addr_t new_page;

    new_page = (addr_t)get_free_page();
    if (!new_page) {
        YLOG_E("irq init failed.\n");
        return;
    }

    irq_sp = new_page;
    YLOG_D("irq_sp=%X\n", irq_sp);

#ifdef CONFIG_GIC_400
    gic_init();
#endif

    return;
}