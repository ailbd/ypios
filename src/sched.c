/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "sched.h"
#include "yList.h"
#include "printf.h"
#include "mm.h"
#include "log.h"
#include "irq.h"
#include "systimer.h"

struct task_struct kernel_boot = INIT_TASK();
struct task_struct *current = &(kernel_boot);
static ylist_head priority_list[SCHED_INVALDk];
u64 pid_incresed = 0;


YLIST_HEAD(tasks_list);

void sched_init() {
    YLOG_T("schedular initilizing...\n");
    int i = 0;
    while (i < SCHED_INVALDk) {
        YLIST_NODE_INIT(priority_list[i]);
        i++;
    }

    init_current();

    sched_enqueue(current);
    YLOG_T("schedular initilizing.\n");
}

void sched_enqueue(struct task_struct *task) {
    YLOG_T("schedular enqueue task\n");
    if (!task) {
        YLOG_E("enqueue task failed.\n");
        return;
    }

    ylist_add(&priority_list[task->priority], &task->prio_list);
    ylist_add(&tasks_list, &task->tasks_list);
    
    task->preemptible = 1;

    YLOG_T("schedular enqueue task OK.\n");
}

void init_current() {
    YLOG_T("Initiate current task of kernel bool.\n");
    if (!current) {
        current = get_free_page();
        printf("init current:%x\n", current);
    }

    init_task(current);

    // get kernel boot context
    init_cpu_context(&current->cpu_context);

    sprintf(&current->name, "%s" "kernel_boot");

    current->pid = get_pid();
    current->state = TT_RUNNING;
    current->priority = SCHED_HIGHT;
    current->preemptible = 0;
    current->time_slices = 10;

    YLOG_T("Initiate current OK.\n");
}

struct task_struct *get_task_by_pid(pid_t pid) {
    ylist_head *taskhead = NULL;
    struct task_struct *task = NULL;

    ylist_foreach(taskhead, &tasks_list) {
        task = ylist_entry(taskhead, struct task_struct, tasks_list);

        if (task->pid == pid) {
            break;
        }
    }

    return task;
}

void init_task(struct task_struct *task) {
    memzero(&task->cpu_context, sizeof(struct cpu_context));

    task->parent = NULL;
    task->pid = INVALID_PID;
    task->time_slices = 0;
    task->state = TT_DESTROYED;
    task->preemptible = 0;
    task->resved = 0;

    YLIST_NODE_INIT(task->prio_list);
    YLIST_NODE_INIT(task->tasks_list);
}

void preempt_disable(void) {
    YLOG_T("change current %x preemptible %d to 0\n",
        current, current->preemptible);

    current->preemptible = 0;
}

void preempt_enable(void) {
    YLOG_T("change current %x preemptible %d to 1\n",
        current, current->preemptible);
    current->preemptible = 1;
}

/**
 * @brief Get the next task object
 *        tasks_list is not empty. that's guaranted by schedule().
 * @return struct task_struct* 
 */
static struct task_struct *get_next_task() {
	ylist_head * next = current->tasks_list.next;

    if (next == &tasks_list) {
        next = next->next;
    }

    struct task_struct *task = ylist_entry(next, struct task_struct,
        tasks_list);

    return task;
}

void schedule() {
    YLOG_T("schedule enter. current %x preemptible =%d\n",
        current, current->preemptible);

    if (!current->preemptible) {
        return;
    }

    if (ylist_isempty(&tasks_list)) {
        YLOG_E("Error, tasks list is empty.\n");
        return;
    };

	preempt_disable();
    struct task_struct * prev_task = NULL;
	struct task_struct * next_task = get_next_task();

    if (!next_task) {
        YLOG_D("no other task need to sched.\n");
        preempt_enable();
        return;
    }

    // only one task in tasks list, do not switch cpu context.
    if (current == next_task) {
        return;
    }

    ymemcpy(&current->cpu_context, irq_sp, sizeof(struct cpu_context));
	prev_task = current;
    current = next_task;
    clean_irq_stack();

    YLOG_D("switching task %X to task %X\n", prev_task, next_task);

    if (!current->cpu_context.spsr_el1) {
        get_spsr_el1(&(current->cpu_context.spsr_el1));
        YLOG_D("current->cpu_context.spsr_el1=%x\n", current->cpu_context.spsr_el1);
    }
    ymemcpy(irq_sp, &current->cpu_context, sizeof(struct cpu_context));

	preempt_enable();
}

void prepare_schedual() {
    u32 cur_count = systimer_get_ticks_low32();

    set_systimer(1, SCHED_TIME_SLICE);
}