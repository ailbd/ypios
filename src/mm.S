/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

.global memzero

// funcion memzero(begin, size)
memzero:
    str xzr, [x0], #8 // this store zero register into memory of address x0
    subs x1, x1, #8 // https://stackoverflow.com/questions/54417869/what-would-be-a-reason-to-use-adds-instruction-instead-of-the-add-instruction-in
                    // !!!subs adds etc will update CPSR flags while sub/add etc will not.
    b.gt memzero // b.gt performs an unditional branch to a target address if the "greater than" condition is true.
                 // The condition is based on the state of condition flags in CPSR(current program status register).
                 // Specifically, the b.gt instruction branches if the Z(zero) flags is clear, whic means the result of 
                 // previous instruction was not zero.
    ret


.global ymemcpy
// function ymemcpy(source, dest, size)
ymemcpy:
    // Inputs:
    //   x0 = destination address
    //   x1 = source address
    //   x2 = size (number of bytes to copy)

    // Check if size is zero, if so, return
    CBZ x2, end_memcpy

    // Main loop for copying data
    copy_loop:
        LDRB w3, [x1], #1  // Load a byte from the source and increment the source pointer
        STRB w3, [x0], #1  // Store the byte to the destination and increment the destination pointer
        SUBS x2, x2, #1    // Decrement the size
        B.NE copy_loop     // Branch back if size is not zero

    end_memcpy:
        RET