/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "mm.h"
#include "sched.h"
#include "log.h"

/**
 * @brief fork process from parent of pid @param pid
 * 
 * @param task the task fored out
 * @param pid parent pid
 * @param attr fork attribute
 * @return int return 0 on success, otherwise negative.
 * 
 * rc = fork(task, xPID, 0);
 * if (rc)
 *     return;
 * if (get_current_pid() != xPID) {
 *     // do sub process task.
 * }
 */
int fork(struct task_struct *task, pid_t pid, u32 attr) {
	struct task_struct * parent = NULL;

	if (!current | !task) {
		YLOG_F("error params, fork failed.\n");
		return -1;
	}

	if (pid != INVALID_PID)	{
		parent = get_task_by_pid(pid);
	} else {
		parent = current;
	}

	if (!parent) return -1;

	preempt_disable();

	task->parent = parent;
	task->priority = parent->priority;
	task->state = TT_CREATED;
	task->pid = get_pid();
	// task->cpu_context = parent->cpu_context;	// ** this will use memcpy
	ymemcpy(&task->cpu_context, &parent->cpu_context,
		sizeof(struct cpu_context));

	task->cpu_context.sp = (unsigned long)task + THREAD_SIZE;

	preempt_enable();
	return 0;
}