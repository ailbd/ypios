/*
 * Copyright (c) 2023 Jianjun Yue and ypios contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "irq.h"
#include "mini_uart.h"
#include "printf.h"
#include "log.h"
#include "sched.h"
#include "kthread.h"

void ypios_main(void)
{
    muart_init();
    init_printf(NULL, putc);
    irq_init_vectors();
    irq_init();
    // initiate dma controller
    dma_init();
    sched_init();
    // unmask DAIF
    // system timer 1 and 3 initiate
    systimer_init() ;

    printf("Welcome yPIOS\n");
    printf("yPIOS version 1.0.0\n");
    printf("Time 1999-01-01\n");

    printf("current exception level: %d\n", get_cel());
    kthread_t thread1, thread2;

    kthread_create(&thread1, kthread_test_func1, "test1", NULL);
    kthread_create(&thread1, kthread_test_func2, "test2", NULL);

    irq_enable();
    u32 counter = 0;
    while (1) {
        counter++;

        if (!(counter % 100000)) {
            YLOG_T("systimer ticks:%X, timer1:%X\n", systimer_get_ticks_low32(),
                get_systimer_channel(1));
            printf("thread %s heartbeats\n", current->name);
        }
    }
}
