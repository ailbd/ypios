# yPiOS

#### 介绍
ypios 是针对raspberry pi4的纯机器代码，芯片是BCM2711，arm架构是armv8-a，并使用uboot引导ypios的启动。
本项目主要是用于学习实践操作系统内核的实现以及底层硬件驱动实现。

#### 环境依赖
1.  raspberry pi 4B+
2.  USB-to-TTL
3.  sd-card
4.  deepin/ubuntu/<linux...> host

#### 使用说明
1.  git clone <this repository>
2.  make (根据需要更改./Makefile TFTP-SERVER路径)
3.  使用makebootfs.sh格式化分区（请注意sd-card会被格式化）
4.  make install往sdcard安装镜像
5.  插入raspberrypi，启动
6.  后续可以通过uboot进行更换镜像

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
