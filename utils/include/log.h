/*
 * Copyright (c) 2023 Jianjun Yue and letscode contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#ifndef _YLOG_H_
#define _YLOG_H_

#include "printf.h"

#ifndef YLOG_TAG
#define YLOG_TAG "YLOG"
#endif

#ifndef __FILENAME__
#define __FILENAME__ "NULL"
#endif

#define YLOG_CLOSE 0
#define YLOG_FATAL 11
#define YLOG_ERR 12
#define YLOG_WARN 13
#define YLOG_INFO 14
#define YLOG_DBG 15
#define YLOG_TRACE 16

#ifndef YLOG_LEVEL
#define YLOG_LEVEL YLOG_TRACE
#endif

#define YLOG(lv, fmt, ...)                                                \
    if (lv <= YLOG_LEVEL) {                                               \
        printf("[%s %s:%s:%d]: ", #lv, __FILENAME__, __func__, __LINE__); \
        printf(fmt, ##__VA_ARGS__);                                       \
    }

#define YLOG_F(fmt, ...) YLOG(YLOG_FATAL, fmt, ##__VA_ARGS__)
#define YLOG_E(fmt, ...) YLOG(YLOG_ERR, fmt, ##__VA_ARGS__)
#define YLOG_W(fmt, ...) YLOG(YLOG_WARN, fmt, ##__VA_ARGS__)
#define YLOG_I(fmt, ...) YLOG(YLOG_INFO, fmt, ##__VA_ARGS__)
#define YLOG_D(fmt, ...) YLOG(YLOG_DBG, fmt, ##__VA_ARGS__)
#define YLOG_T(fmt, ...) YLOG(YLOG_TRACE, fmt, ##__VA_ARGS__)

// 按[%d, %d, ...]格式打印整型数组
#define PRINT_INT_ARR(arr, len)                     \
            printf("[");                            \
            for (int i = 0; i < (len); i++) {       \
                if (i == (len) - 1) {               \
                    printf("%d", *((arr) + i));     \
                } else {                            \
                    printf("%d,", *((arr) + i));    \
                }                                   \
            }                                       \
            printf("]\n");

#endif
