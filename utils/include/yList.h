/*
 * Copyright (c) 2023 Jianjun Yue and letscode contributors.
 * letscode is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#ifndef _YLIST_H_
#define _YLIST_H_

typedef struct ylist_head {
    struct ylist_head *prev, *next;
} ylist_head;

#define YLIST_HEAD_INIT(head) { &(head), &(head) }
#define YLIST_HEAD(name) \
    static ylist_head name = YLIST_HEAD_INIT(name)
#define YLIST_NODE_INIT(node) \
    (node).prev = (node).next = &(node);

static inline int ylist_isempty(ylist_head *head)
{
    return head->prev == head;
}

// do not call this directly
static inline void ylist__add_node(ylist_head *node, ylist_head *prev, ylist_head *next)
{
    prev->next = node;
    node->prev = prev;
    node->next = next;
    next->prev = node;
}

// do not call this directly
static inline void ylist__del_node(ylist_head *prev, ylist_head *next)
{
    prev->next = next;
    next->prev = prev;
}

static inline void ylist_add(ylist_head *head, ylist_head *node)
{
    ylist__add_node(node, head->prev, head);
}

static inline void ylist_insert(ylist_head *prev, ylist_head *node)
{
    ylist__add_node(node, prev, prev->next);
}

static inline void ylist_delete(ylist_head *node)
{
    ylist__del_node(node->prev, node->next);
}

// remove @node from a list, and then add it into another list
static inline void ylist_move(ylist_head *node, ylist_head *head)
{
    ylist_delete(node);
    ylist_add(head, node);
}

#undef ylist_offsetof
#ifdef __compiler_offsetof
#define ylist_offsetof(TYPE, MEMBER) (__compiler_offsetof(TYPE, MEMBER))
#else
#define ylist_offsetof(TYPE, MEMBER) ((int)(unsigned long)&((TYPE *)0)->MEMBER)
#endif

#define container_of(ptr, type, member) ({                       \
    __typeof__(((type *)0)->member) *__mptr = (ptr);             \
    (type *)((char *)__mptr - (ylist_offsetof(type, member))); })

#define ylist_entry(ptr, type, member) container_of(ptr, type, member)

#define ylist_foreach(pos, head) \
    for ((pos) = (head)->next; (pos) != (head); (pos) = (pos)->next)

#define ylist_foreach_r(pos, head) \
    for ((pos) = (head)->prev; (pos) != (head); (pos) = (pos)->prev)

#endif
