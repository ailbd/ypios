#!/bin/bash

set -e
set -x

# Check if the script is run as root
if [ "$EUID" -ne 0 ]; then
  echo "Please run as root"
  exit 1
fi

sdcard=$1
bootfs_mount_point="/mnt/boot"
kernel_mount_point="/mnt/kernel"

exist=$(lsblk |grep "${sdcard##*/}")

if [ -z $exist ];then
    echo "Please ensure $sdcard exists."
    exit 1
fi

set +e
if [ -d $bootfs_mount_point ];then
    umount $bootfs_mount_point
else
    mkdir -p $bootfs_mount_point
fi
mount ${sdcard}1 $bootfs_mount_point

if [ -d $kernel_mount_point ];then
    umount $kernel_mount_point
else
    mkdir -p $kernel_mount_point
fi
set -e

mount ${sdcard}2 $kernel_mount_point

make
make install BOOTFS_MNT=$bootfs_mount_point KERNEL_MNT=$kernel_mount_point
make clean

umount $bootfs_mount_point
umount $kernel_mount_point

# eject $sdcard